package ru.pisarev.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.event.ConsoleEvent;
import ru.pisarev.tm.listener.AbstractListener;

@Component
public class ExitListener extends AbstractListener {
    @Override
    public String name() {
        return "exit";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Close application.";
    }

    @Override
    @EventListener(condition = "@exitListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.exit(0);
    }
}
