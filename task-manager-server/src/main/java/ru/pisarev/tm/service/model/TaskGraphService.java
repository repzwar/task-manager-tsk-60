package ru.pisarev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.pisarev.tm.api.repository.model.ITaskRepository;
import ru.pisarev.tm.api.service.model.ITaskService;
import ru.pisarev.tm.enumerated.Status;
import ru.pisarev.tm.exception.empty.EmptyIdException;
import ru.pisarev.tm.exception.empty.EmptyIndexException;
import ru.pisarev.tm.exception.empty.EmptyNameException;
import ru.pisarev.tm.exception.entity.TaskNotFoundException;
import ru.pisarev.tm.model.TaskGraph;
import ru.pisarev.tm.model.UserGraph;

import java.util.Collection;
import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class TaskGraphService extends AbstractGraphService<TaskGraph> implements ITaskService {

    @NotNull
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll() {
        return repository.findAll();
    }

    @Override
    @SneakyThrows
    public void addAll(@Nullable final Collection<TaskGraph> collection) {
        if (collection == null) return;
        for (TaskGraph item : collection) {
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@Nullable final TaskGraph entity) {
        if (entity == null) return null;
        repository.add(entity);
        return entity;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findById(optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final List<TaskGraph> tasks = repository.findAll();
        for (TaskGraph t :
                tasks) {
            repository.remove(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final TaskGraph task = repository.getReference(optionalId.orElseThrow(EmptyIdException::new));
        if (task == null) return;
        repository.remove(task);
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final TaskGraph entity) {
        if (entity == null) return;
        @Nullable final TaskGraph task = repository.getReference(entity.getId());
        if (task == null) return;
        repository.remove(task);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        return repository.findByIndex(userId, index);
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        return repository.findByName(userId, name);
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @Nullable final TaskGraph task = repository.findByIndex(userId, index);
        if (task == null) return;
        repository.remove(task);
    }

    @Override
    @SneakyThrows
    public void removeByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskGraph task = repository.findByName(userId, name);
        if (task == null) return;
        repository.remove(task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateById(
            @NotNull final String userId, @Nullable final String id,
            @Nullable final String name, @Nullable final String description) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph updateByIndex(
            @NotNull final String userId, @Nullable final Integer index,
            @Nullable final String name, @Nullable final String description) {
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setName(name);
        task.setDescription(description);
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph startByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.IN_PROGRESS);
        task.setStartDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishById(@NotNull final String userId, @Nullable final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIdUserId(userId, id))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByIndex(@NotNull final String userId, @Nullable final Integer index) {
        if (index == null || index < 0) throw new EmptyIndexException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByIndex(userId, index))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.update(task);
        return task;
    }

    @NotNull
    @Override
    @SneakyThrows
    public TaskGraph finishByName(@NotNull final String userId, @Nullable final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NotNull final TaskGraph task = Optional.ofNullable(repository.findByName(userId, name))
                .orElseThrow(TaskNotFoundException::new);
        task.setStatus(Status.COMPLETED);
        task.setFinishDate(new Date());
        repository.update(task);
        return task;
    }

    @SneakyThrows
    @Nullable
    public TaskGraph add(@NotNull UserGraph user, @Nullable String name, @Nullable String description) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @Nullable final TaskGraph task = new TaskGraph(name, description);
        add(user, task);
        return (task);
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<TaskGraph> findAll(@NotNull final String userId) {
        return repository.findAllByUserId(userId);
    }

    @Override
    @SneakyThrows
    public void addAll(@NotNull final UserGraph user, @Nullable final Collection<TaskGraph> collection) {
        if (collection == null || collection.isEmpty()) return;
        for (TaskGraph item : collection) {
            item.setUser(user);
            add(item);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph add(@NotNull final UserGraph user, @Nullable final TaskGraph entity) {
        if (entity == null) return null;
        entity.setUser(user);
        @Nullable final TaskGraph entityResult = add(entity);
        return entityResult;
    }

    @Nullable
    @Override
    @SneakyThrows
    public TaskGraph findById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        return repository.findByIdUserId(userId, optionalId.orElseThrow(EmptyIdException::new));
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        repository.clearByUserId(userId);
        @NotNull final List<TaskGraph> tasks = repository.findAllByUserId(userId);
        for (TaskGraph t :
                tasks) {
            repository.remove(t);
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final Optional<String> optionalId = Optional.ofNullable(id);
        @Nullable final TaskGraph task = repository.findByIdUserId(
                userId,
                optionalId.orElseThrow(EmptyIdException::new)
        );
        if (task == null) return;
        repository.remove(task);
    }

    @Override
    @SneakyThrows
    public void remove(@NotNull final String userId, @Nullable final TaskGraph entity) {
        if (entity == null) return;
        @Nullable final TaskGraph task = repository.findByIdUserId(userId, entity.getId());
        if (task == null) return;
        repository.remove(task);
    }

}