package ru.pisarev.tm.component;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.pisarev.tm.listener.LogMessageListener;
import ru.pisarev.tm.service.ReceiverService;

@Component
public final class Bootstrap {

    @NotNull
    @Autowired
    ReceiverService receiverService;

    public void start() {
        receiverService.receive(new LogMessageListener());
    }

}
